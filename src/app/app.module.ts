import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MatchcardsComponent } from './components/matchcards/matchcards.component';
import { CasinoComponent } from './components/casino/casino.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { RightsidebarComponent } from './components/rightsidebar/rightsidebar.component';
import { PromotionComponent } from './components/promotion/promotion.component';
import { RulesComponent } from './components/rules/rules.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    MatchcardsComponent,
    CasinoComponent,
    HomepageComponent,
    RightsidebarComponent,
    PromotionComponent,
    RulesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
