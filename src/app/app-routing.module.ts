import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasinoComponent } from './components/casino/casino.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { PromotionComponent } from './components/promotion/promotion.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'casino', component: CasinoComponent },
  { path: 'promotion', component: PromotionComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
