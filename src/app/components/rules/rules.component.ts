import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  hideRules(){
    var rulesContainer = document.querySelector('.rule-container');
    rulesContainer.classList.toggle('show');
  }
}
